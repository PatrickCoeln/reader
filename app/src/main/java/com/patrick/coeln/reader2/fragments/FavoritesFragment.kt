package com.patrick.coeln.reader2.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListView
import androidx.fragment.app.Fragment
import com.patrick.coeln.reader2.R
import com.patrick.coeln.reader2.controller.FavoriteController
import com.patrick.coeln.reader2.mainActivity


class FavoritesFragment: Fragment() {

    private val pFavoritesList: ExpandableListView by lazy {
        mainActivity().findViewById<ExpandableListView>(R.id.favorites_list)
    }

    val favoritesListView: ExpandableListView
        get() = pFavoritesList

    private lateinit var favoriteController: FavoriteController



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return view?.let {
            it
        }?: inflater.inflate(R.layout.favorite_layout, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        favoriteController = FavoriteController(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        favoriteController.cleanUp()
    }
}