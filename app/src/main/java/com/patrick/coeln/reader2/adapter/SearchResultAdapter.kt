package com.patrick.coeln.reader2.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter

import android.widget.ImageButton
import com.google.android.material.textview.MaterialTextView
import com.patrick.coeln.reader2.R
import com.patrick.coeln.reader2.controller.SearchController
import com.patrick.coeln.reader2.model.Manga

class SearchResultAdapter(private val context: Context,private val controller: SearchController): BaseAdapter() {
    private val mangas = ArrayList<Manga>()
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return with(context.getSystemService(LayoutInflater::class.java)) {
            inflate(R.layout.search_item, parent,false).apply {
                findViewById<MaterialTextView>(R.id.manga_name).apply {
                    text = mangas[position].name
                }
                findViewById<ImageButton>(R.id.imageButton).apply {
                    setOnClickListener {
                        controller.writeMangaToPref(mangas[position])
                    }
                }
            }
        }
    }

    override fun getItem(position: Int) = mangas[position]

    override fun getItemId(position: Int) = 0L

    override fun getCount(): Int = mangas.size

    fun addList(list: ArrayList<Manga>) {
        mangas.clear()
        mangas.addAll(list)
        notifyDataSetChanged()
    }
}