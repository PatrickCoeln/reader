package com.patrick.coeln.reader2.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.patrick.coeln.reader2.R
import com.patrick.coeln.reader2.controller.SettingsController

class SettingsFragment: Fragment() {

    private val controller: SettingsController by lazy {
        SettingsController(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.settings_layout, container, false)
    }
}