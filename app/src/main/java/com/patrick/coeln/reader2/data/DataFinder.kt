package com.patrick.coeln.reader2.data

import com.patrick.coeln.reader2.extensions.easyFilter

import com.patrick.coeln.reader2.model.Chapter
import com.patrick.coeln.reader2.model.Manga
import kotlinx.coroutines.*
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.io.FileOutputStream
import java.net.HttpURLConnection
import java.net.URL

class DataFinder private constructor() {

    object DataSingleton {
        val finder: DataFinder = DataFinder()
    }

    suspend fun buildMangasAsync(mangaName: String, mangaAddress: Addresses = Addresses.MANGA_READER) = coroutineScope {
        val addresses = findMangaByNameAsync(
            mangaAddress.mName + "/" to mangaName
        ).await()
        val chapters = ArrayList<Deferred<Chapter>>()
        for ((counter, i) in addresses.first.withIndex()) {
            chapters.add( async { (Chapter(i, mangaName + i + counter)) })
        }


        return@coroutineScope Manga(mangaName, chapters.awaitAll().toCollection(ArrayList()))
    }

    @Suppress("Unchecked")
    private suspend fun findMangaByNameAsync(urlName: Pair<String, String>) = coroutineScope {
        // formats url and manga name for query
        val url = if(urlName.first.endsWith("/")) urlName.first.toLowerCase() else urlName.first.toLowerCase()+"/"
        val manga = urlName.second.trimStart().trimEnd().toLowerCase().replace(' ','-')
        async(Dispatchers.IO) {
            val resultPair = Pair<ArrayList<String>, ArrayList<String?>>(ArrayList(), ArrayList())
            val links = ArrayList<String?>()
            val doc: Document = Jsoup.connect(url+manga).get()
            val elem = doc.getElementsByTag(TAGS.A_TAG.tagName) // fetch every a tag
            elem.forEach {
                links.add(it.easyFilter { item->
                    item.contains(manga)
                })
            }

            val filtered = links.asSequence()
                .filter {item-> item != null}
                .map {item-> item?.replace(TAGS.HREF.tagName,"")}
                .map {item-> item?.replace("\"","")}
                .map {item-> item?.replace(">"," ")}
                .map {item->
                    resultPair.second.add(item?.substringAfter(' ', "<")?.also { it.substring(0, it.length - 4) })
                    item?.replaceAfter(" ","")
                }
                .map { item-> item?.replaceFirst("/", url) }
                .sortedBy {
                    val x = it!!.lastIndexOf('/', it.length)
                    it.trim().substring(x + 1,it.length -1).toInt()
                }
                .distinct()
                .toCollection(ArrayList())
            @Suppress("UNCHECKED_CAST") // objects went through filter and are therefore not null shitty type erasure (Thanks Merkel)
            resultPair.first.addAll(filtered as ArrayList<String>)

            return@async resultPair
        }
    }

    /**
     * can be used to load complete chapters async
     * asynchronous saves time in this context
     */
    @Suppress("unused")
    suspend fun loadImageWithStdAsync(url: String, fileName: String) = coroutineScope {
        async(Dispatchers.IO) {
            val fileExtension = url.substring(url.length - 4,url.length)
            with(URL(url)) {
                val httpConnect = (openConnection() as HttpURLConnection).apply {  // tricks site and allows downloading data from it
                    addRequestProperty("User-Agent", "Mozilla/5.0")
                }
                val inStream = httpConnect.inputStream
                val fileOut = FileOutputStream(fileName+fileExtension)
                val buffOut = fileOut.buffered()
                val bytes = ByteArray(8192)
                var bRead: Int
                while (true) {
                    bRead = inStream.read(bytes)
                    if (bRead <= 0)
                        break
                    else
                        buffOut.write(bytes,0,bRead)
                }
                fileOut.flush()
                fileOut.close()
            }
        }
    }

    /**
     * Gets absolute URL of img
     * will wait for 15 seconds until timeout
     */
    suspend fun fetchImageAddress(url: String, id: String = "img", key: String = "src"): String = coroutineScope {
        val doc: Document = withContext(Dispatchers.IO) {Jsoup.connect(url).timeout(15000).get()}
        val elem = doc.getElementById(id)
        elem.absUrl(key)
    }

}