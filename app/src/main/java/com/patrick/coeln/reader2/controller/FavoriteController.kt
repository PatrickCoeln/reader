package com.patrick.coeln.reader2.controller

import android.content.Context
import com.google.gson.Gson
import com.patrick.coeln.reader2.adapter.ExpandableAdapter
import com.patrick.coeln.reader2.constants.MANGA_PREFS
import com.patrick.coeln.reader2.fragments.FavoritesFragment
import com.patrick.coeln.reader2.mainActivity
import com.patrick.coeln.reader2.model.Manga
import kotlinx.coroutines.*

class FavoriteController(private val favoritesFragment: FavoritesFragment): BaseController() {


    private val mainRef = favoritesFragment.mainActivity()


    private lateinit var favoriteAdapter: ExpandableAdapter

    init {
        initListView()
    }

    private suspend fun getMangaData() = coroutineScope  {
        val mangas = mainRef.getSharedPreferences(MANGA_PREFS, Context.MODE_PRIVATE).all
        val mangaData = ArrayList<Deferred<Manga>>()
        val result = ArrayList<Manga>()
        for (item in mangas.entries) {
            mangaData.add(async(Dispatchers.Default) {Gson().fromJson(item.value.toString(), Manga::class.java)})
        }
        result.addAll(
            withContext(Dispatchers.Default) {mangaData.awaitAll()}
        )
        return@coroutineScope result
    }

    fun launchRead() {
        mainRef.startRead()
    }

    private fun initListView() {
        launch {
            favoriteAdapter =  ExpandableAdapter(mainRef,getMangaData(), this@FavoriteController)
            favoritesFragment.favoritesListView.setAdapter(favoriteAdapter)
        }

    }
}