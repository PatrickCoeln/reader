package com.patrick.coeln.reader2.controller

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext


/**
 * provides CoroutineScope and simple cleanup functions
 */

open class BaseController: CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    private fun closeScope() = job.cancel()

    open fun cleanUp() {
        closeScope()
    }
}