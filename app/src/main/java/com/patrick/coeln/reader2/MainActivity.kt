package com.patrick.coeln.reader2


import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.patrick.coeln.reader2.constants.READ_TAG
import com.patrick.coeln.reader2.controller.MainController
import com.patrick.coeln.reader2.extensions.connected

fun Fragment.mainActivity() = this.activity as MainActivity
fun Fragment.isConnected() = mainActivity().connected
class MainActivity : AppCompatActivity() {


    private lateinit var controller: MainController
    private val pBottomView: BottomNavigationView by lazy {
        findViewById<BottomNavigationView> (
            R.id.bottomNavigationView
        )
    }
    val bottomView: BottomNavigationView
        get() = pBottomView

    fun startRead() {
        controller.startRead()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        supportFragmentManager.popBackStack(READ_TAG,FragmentManager.POP_BACK_STACK_INCLUSIVE)
        bottomView.selectedItemId = R.id.favorites_item
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        controller = MainController(this)
    }
}
