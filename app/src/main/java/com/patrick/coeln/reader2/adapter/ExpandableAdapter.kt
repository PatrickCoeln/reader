package com.patrick.coeln.reader2.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import com.google.android.material.textview.MaterialTextView
import com.google.gson.Gson
import com.patrick.coeln.reader2.R
import com.patrick.coeln.reader2.constants.SELECTED
import com.patrick.coeln.reader2.constants.SELECTED_MANGA
import com.patrick.coeln.reader2.controller.FavoriteController
import com.patrick.coeln.reader2.data.ActChapter
import com.patrick.coeln.reader2.model.Chapter
import com.patrick.coeln.reader2.model.Manga

import kotlinx.coroutines.launch

class ExpandableAdapter(private val c: Context, private val manga: ArrayList<Manga>, private val controller: FavoriteController): BaseExpandableListAdapter() {


    private val mangaMap: HashMap<String, ArrayList<Chapter>> by lazy {
        getHashMap()
    }

    private fun getHashMap(): HashMap<String, ArrayList<Chapter>> {
        val hashMap = HashMap<String, ArrayList<Chapter>>()
        for(i in manga) {
            hashMap[i.name] = i.chapters
        }
        return hashMap
    }

    override fun getGroup(groupPosition: Int) = manga[groupPosition]

    override fun isChildSelectable(groupPosition: Int, childPosition: Int) = true

    override fun hasStableIds() = false

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View {
        val header = getGroup(groupPosition).name

        val inflater: LayoutInflater = c.getSystemService(LayoutInflater::class.java) as LayoutInflater

        return inflater.inflate(R.layout.favorite_list_item,  parent,false).apply {
            findViewById<MaterialTextView>(R.id.favorites_item).apply {
                text = header
            }
        }
    }

    override fun getChildrenCount(groupPosition: Int): Int = manga[groupPosition].chapters.size

    override fun getChild(groupPosition: Int, childPosition: Int) = mangaMap[manga[groupPosition].name]?.get(childPosition)

    override fun getGroupId(groupPosition: Int): Long = groupPosition.toLong()

    override fun getChildView(
        groupPosition: Int,
        childPosition: Int,
        isLastChild: Boolean,
        convertView: View?,
        parent: ViewGroup?
    ): View {
        val chapter = getChild(groupPosition, childPosition)
        val chapterName = chapter?.name?.replaceAfter("https","")
            ?.replace("https"," ${childPosition + 1}")

        val inflater: LayoutInflater = c.getSystemService(LayoutInflater::class.java) as LayoutInflater
        return inflater.inflate(R.layout.favorite_list_item, parent,false).apply {
            findViewById<MaterialTextView>(R.id.favorites_item).apply {
                text = chapterName
            }.apply {
                setOnClickListener {
                    controller.launch {
                        setUpForRead(groupPosition, childPosition)
                    }
                }
            }
        }
    }

    /**
     * save Manga and selected Chapter and launch new Fragment
     */
    private fun setUpForRead(groupPosition: Int, childPosition: Int)  {
        c.getSharedPreferences(SELECTED, Context.MODE_PRIVATE).apply {
            edit().apply {
                putString(SELECTED_MANGA, Gson().toJson(manga[groupPosition]))
                ActChapter.siteNum = childPosition
            }.apply()
        }
        controller.launchRead()
    }


    override fun getChildId(groupPosition: Int, childPosition: Int) = childPosition.toLong()


    override fun getGroupCount() = manga.size

}