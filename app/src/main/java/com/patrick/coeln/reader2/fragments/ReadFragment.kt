package com.patrick.coeln.reader2.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.github.chrisbanes.photoview.PhotoView
import com.patrick.coeln.reader2.R
import com.patrick.coeln.reader2.controller.ReadController
import com.patrick.coeln.reader2.mainActivity



class ReadFragment: Fragment() {
    private val pPhotoView: PhotoView by lazy {
        mainActivity().findViewById<PhotoView>(R.id.read_photo_view)
    }
    val photoView: PhotoView
        get() = pPhotoView

    private lateinit var readController: ReadController



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.read_layout, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        readController = ReadController(this)
    }

}