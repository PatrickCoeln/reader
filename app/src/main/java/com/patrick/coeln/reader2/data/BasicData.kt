package com.patrick.coeln.reader2.data

import java.io.Serializable

@Suppress("unused")
enum class Addresses(val mName: String): Serializable {
    MANGA_READER("https://www.mangareader.net"),
    MANGA_PANDA("https://www.mangapanda.com")
}
@Suppress("unused")
enum class TAGS(val tagName: String) {
    MA_PAGEMENU("pageMenu"),
    A_TAG("a"),
    HREF("<a href=\"")
}