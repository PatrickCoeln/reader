package com.patrick.coeln.reader2.controller

import android.content.Context
import com.google.gson.Gson
import com.patrick.coeln.reader2.R
import com.patrick.coeln.reader2.constants.*
import com.patrick.coeln.reader2.data.ActChapter
import com.patrick.coeln.reader2.extensions.toast
import com.patrick.coeln.reader2.fragments.ReadFragment
import com.patrick.coeln.reader2.isConnected
import com.patrick.coeln.reader2.mainActivity
import com.patrick.coeln.reader2.model.Chapter
import com.patrick.coeln.reader2.model.Manga
import com.squareup.picasso.Picasso
import kotlinx.coroutines.*
import kotlin.math.abs


class ReadController(private val read: ReadFragment): BaseController() {
    private val mainRef = read.mainActivity()

    private var actSite: Int = 0
    private var chapterNum: Int = 0

    private lateinit var selectedManga: Manga
    private lateinit var actChapter: Chapter


    init {
        initPhotoView()
        launch {
            if (read.isConnected()) {
                initMangaAsync().await()
                loadToImageView(actChapter.sites[actSite].address)
            } else {
                launch(Dispatchers.Main) {mainRef.apply { toast(getString(R.string.no_connection)) }
                }
            }
        }
    }


    private suspend fun loadPrevious() = coroutineScope{
        if (chapterNum - 1 >= 0) {
            chapterNum -= 1
            setActChapterAsync(chapterNum).await()
            actSite = 0
            loadToImageView(actChapter.sites[actSite].address)
        }else {
            launch(Dispatchers.Main) {
                mainRef.apply { toast(getString(R.string.no_page_before)) }
            }
        }
    }

    private suspend fun loadNext() = coroutineScope {
        if (chapterNum + 1 <= selectedManga.chapters.size) {
            chapterNum += 1
            setActChapterAsync(chapterNum).await()
            actSite = 0
            loadToImageView(actChapter.sites[actSite].address)
        } else {
            mainRef.apply { toast(getString(R.string.no_page_next)) }
        }
    }

    private fun swipeLeft() {
        if (read.isConnected()) {
            launch {
                actSite -= 1
                if (actSite < 0) {
                    launch {
                        runCatching {
                            loadPrevious()
                        }
                    }

                } else {
                    runCatching { loadToImageView(actChapter.sites[actSite].address) }
                }
            }
        } else {
            launch { mainRef.apply { toast(getString(R.string.no_connection)) }
            }
        }
    }

    private fun swipeRight() {
        if (read.isConnected()) {
            launch {
                actSite += 1
                if (actSite == actChapter.sites.size) {
                    launch {
                        runCatching {
                            loadNext()
                        }
                    }
                } else {
                    runCatching {
                        loadToImageView(actChapter.sites[actSite].address)
                    }
                }
            }
        } else {
            launch { mainRef.apply { toast(getString(R.string.no_connection)) }
            }
        }
    }

    private fun initPhotoView() {
        read.photoView.apply {
            maximumScale = 3f
            mediumScale = 1.8f
            minimumScale = 1f

            setOnSingleFlingListener { e1, e2, _, _ ->
                val minSwipeX = 100.0
                val maxSwipeX = 1000.0
                val x1 = e1.x
                val x2 = e2.x


                val deltaX: Float = x1 - x2
                val deltaXAbs: Float = abs(x1 - x2)

                if (deltaXAbs in minSwipeX..maxSwipeX) {
                    if (deltaX > 0) {
                        swipeRight()
                    } else {
                        swipeLeft()
                    }
                }

                true
            }
        }
    }

    private fun loadToImageView(str: String) =
        Picasso.get().load(str).into(read.photoView)

    private suspend fun setActChapterAsync(num: Int) = coroutineScope {
        async {
            selectedManga.chapters[num].fillSites()
            actChapter = selectedManga.chapters[num]
        }
    }



    private suspend fun initMangaAsync() = coroutineScope {
        selectedManga = mainRef.getSharedPreferences(SELECTED,Context.MODE_PRIVATE).let {

            Gson().fromJson(it.getString(SELECTED_MANGA,""), Manga::class.java)
        }.also {
            chapterNum = ActChapter.siteNum
        }
        setActChapterAsync(chapterNum)
    }

}