package com.patrick.coeln.reader2.constants


const val MANGA_PREFS = "MANGA_PREFS"

const val READ_TAG = "READ_FRAGMENT"

const val ADDRESS_KEY = "ADDRESS_KEY"
const val SELECTED = "SELECTED"
const val SELECTED_MANGA = "SELECTED_MANGA"
