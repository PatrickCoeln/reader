package com.patrick.coeln.reader2.extensions

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.widget.Toast
import org.jsoup.nodes.Element


fun Element.easyFilter(func: (String)-> Boolean): String? {
    return if (func(this.toString())) this.toString() else null
}


val Context.connected: Boolean

    get()  {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val netInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            netInfo?.isConnected ?: false
        } else {
            false
        }
    }

fun Context.toast(msg: String, isShort: Boolean = true) = Toast.makeText(this, msg, if(isShort) Toast.LENGTH_SHORT else Toast.LENGTH_LONG).show()