package com.patrick.coeln.reader2.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ListView
import androidx.fragment.app.Fragment
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.patrick.coeln.reader2.R
import com.patrick.coeln.reader2.constants.ADDRESS_KEY
import com.patrick.coeln.reader2.controller.SearchController
import com.patrick.coeln.reader2.data.Addresses
import com.patrick.coeln.reader2.mainActivity

class SearchFragment: Fragment() {


    private lateinit var controller: SearchController
    private val pListView: ListView by lazy {
        mainActivity().findViewById<ListView>(R.id.manga_list)
    }

    val listView: ListView
        get() = pListView

    private val pInput: TextInputEditText by lazy {
        mainActivity().findViewById<TextInputEditText>(
            R.id.textInputEditText
        )
    }
    val input: TextInputEditText
        get() = pInput

    private val pButton: Button by lazy {
        mainActivity().findViewById<MaterialButton>(
            R.id.search_button
        )
    }

    val searchButton: Button
        get() = pButton

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.search_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val dataSource = arguments?.let {
            (it.getSerializable(ADDRESS_KEY) as Addresses)
        } ?: Addresses.MANGA_READER
        controller = SearchController(this, dataSource)
    }
    override fun onDestroy() {
        super.onDestroy()
        controller.cleanUp()
    }

    companion object {
        fun instance(sourceInfo: Addresses?): SearchFragment {
            return SearchFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(
                        ADDRESS_KEY,
                        sourceInfo
                    )
                }
            }
        }
    }
}