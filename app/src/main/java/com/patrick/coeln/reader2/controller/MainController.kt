package com.patrick.coeln.reader2.controller


import androidx.fragment.app.Fragment
import com.patrick.coeln.reader2.MainActivity
import com.patrick.coeln.reader2.R
import com.patrick.coeln.reader2.constants.READ_TAG
import com.patrick.coeln.reader2.data.Addresses

import com.patrick.coeln.reader2.fragments.FavoritesFragment
import com.patrick.coeln.reader2.fragments.ReadFragment
import com.patrick.coeln.reader2.fragments.SearchFragment
import com.patrick.coeln.reader2.fragments.SettingsFragment


class MainController(private val mainActivity: MainActivity) {

    private var dataSource: Addresses = Addresses.MANGA_READER // default data-source if not specified

    init {
        setBottomListener()
        setView(SearchFragment())
    }

    private fun setView(fragment: Fragment) = with(mainActivity.supportFragmentManager) {
        beginTransaction().also {
            it.replace(R.id.main_container, fragment)
            if (fragment is ReadFragment) {
                it.addToBackStack(READ_TAG)
            }
        }.commit()
    }

    fun startRead() {
        setView(ReadFragment())
    }


    private fun setBottomListener() = with (mainActivity.bottomView) {
        setOnNavigationItemSelectedListener {
            when(it.itemId) {
                R.id.search_page -> {
                    setView(SearchFragment.instance(dataSource))
                    true
                }
                R.id.settings_page -> {
                    setView(SettingsFragment())
                    true
                }
                R.id.favorites_page -> {
                    setView(FavoritesFragment())
                    true
                }
                else -> false
            }
        }
    }
}