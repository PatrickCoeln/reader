package com.patrick.coeln.reader2.controller

import android.content.Context
import android.view.View
import com.google.gson.Gson
import com.patrick.coeln.reader2.R
import com.patrick.coeln.reader2.adapter.SearchResultAdapter
import com.patrick.coeln.reader2.constants.MANGA_PREFS
import com.patrick.coeln.reader2.data.Addresses
import com.patrick.coeln.reader2.data.DataFinder
import com.patrick.coeln.reader2.extensions.toast
import com.patrick.coeln.reader2.fragments.SearchFragment
import com.patrick.coeln.reader2.isConnected
import com.patrick.coeln.reader2.mainActivity
import com.patrick.coeln.reader2.model.Manga
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class SearchController(private val searchFragment: SearchFragment, private val dataSource: Addresses = Addresses.MANGA_READER): BaseController() {

    private val mainRef = searchFragment.mainActivity()
    private val dataFinder: DataFinder = DataFinder.DataSingleton.finder

    private val resultAdapter: SearchResultAdapter by lazy {
        SearchResultAdapter(mainRef, this)
    }

    init {
        searchFragment.searchButton.setOnClickListener(addListener())
        searchFragment.listView.adapter = resultAdapter
    }



    private fun processInput() {
        val mangaName = searchFragment.input.text.toString()
            .trim()
            .toLowerCase(Locale.getDefault())
            .replace(' ', '-')

        when(searchFragment.isConnected()) {
            true -> {
                launch {
                    if (checkIfAddressIsValid(mangaName)) {
                        launch {
                            val manga = dataFinder.buildMangasAsync(mangaName,dataSource)
                            resultAdapter.addList(arrayListOf(manga))

                        }
                    } else { launch(Dispatchers.Main) { mainRef.apply{toast(getString(R.string.not_found_data))}}
                    }
                }
            }
            else -> {launch(Dispatchers.Main) { mainRef.apply{toast(getString(R.string.no_connection)) }}}
        }
    }

    private fun checkInput() = searchFragment.input.text.isNullOrBlank()



    private suspend fun checkIfAddressIsValid(mangaName: String): Boolean = withContext(Dispatchers.IO) {
        val site = Addresses.MANGA_READER.mName+"/"
        (URL(site+mangaName).openConnection() as HttpURLConnection).let {httpConn->
            httpConn.requestMethod = "HEAD"
            httpConn.responseCode == 200
        }
    }

    fun writeMangaToPref(manga: Manga ) {
       launch(Dispatchers.Default) {
            mainRef.getSharedPreferences(MANGA_PREFS, Context.MODE_PRIVATE).apply {
                with(edit()) {
                    putString(manga.name,Gson().toJson(manga))
                        .apply()
                }
            }
           launch(Dispatchers.Main) {
               mainRef.apply {toast(getString(R.string.add_to_favorites)) }
           }
        }
    }

    private fun addListener() = View.OnClickListener {
        if (checkInput()) {
            launch(Dispatchers.Main) {
                mainRef.apply {toast(getString(R.string.no_user_data_provided)) }
            }
        } else {
            processInput()
        }
    }
}