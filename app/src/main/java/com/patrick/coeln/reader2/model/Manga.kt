package com.patrick.coeln.reader2.model

import com.patrick.coeln.reader2.data.DataFinder
import com.patrick.coeln.reader2.data.TAGS
import kotlinx.coroutines.*
import org.jsoup.Jsoup
import org.jsoup.nodes.Document


data class Site(val address: String, val pageNum: Int)


class Chapter(private val startAddress: String,val name: String) {
    val sites = ArrayList<Site>()

    suspend fun fillSites() = coroutineScope {
        launch(Dispatchers.Default) {
            val pageCount = findPageCount(TAGS.MA_PAGEMENU.tagName)

            val addresses = ArrayList<Deferred<String>>()

            for (i in 0 until pageCount) {
                addresses.add(async(Dispatchers.Default) {
                    val addressTemp = (startAddress.replace(" ","/"+(i+1).toString()))
                    DataFinder.DataSingleton.finder.fetchImageAddress(addressTemp)
                })
            }

            val finishedAddress = addresses.awaitAll().toCollection(ArrayList())

            for (i in 0 until pageCount){
                sites.add(Site(finishedAddress[i], i+1))
            }
        }
    }

    private suspend fun findPageCount(id: String) = coroutineScope {
        val doc: Document = withContext(Dispatchers.IO) {Jsoup.connect(startAddress).ignoreContentType(true).get()}
        val elem = doc.getElementById(id)
        var temp = elem.toString().replace("[^0-9]".toRegex(),"")
        temp = if(temp.substring(temp.length - 2, temp.length).startsWith("0")) temp.substring(temp.length - 2, temp.length) else temp.substring(temp.length - 2, temp.length)
        temp.toInt()
    }

}

data class Manga(val name: String, val chapters: ArrayList<Chapter>,var chapterPosition: Int = 0, var actSitePosition: Int = 0) 